<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use App\Exports\SiswaExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

//mendeklarasikan model Siswa
use App\Siswa;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
      if ($request->has('cari')) {
        $data_siswa = \App\Siswa::where('nama_depan', 'like', '%'.$request->cari.'%')->get();
      }else {
        $data_siswa = \App\Siswa::all();
      }
      return view('siswa.index', ['data_siswa' => $data_siswa]);
    }

    public function create(Request $request)
    {
      $this->validate($request, [
        'nama_depan'    => 'required|min:3',
        'nama_belakang' => 'required',
        'email'         => 'required|email|unique:users',
        'jenis_kelamin' => 'required',
        'agama'         => 'required',
        'avatar'        => 'mimes:jpeg,jpg,png',
      ]);

      //insert ke table users
      $user = new \App\User;
      $user->role = 'siswa';
      $user->name = $request->nama_depan;
      $user->email = $request->email;
      $user->password = bcrypt('qwerty');
      $user->remember_token = Str::random(60);
      $user->save();

      //insert ke table siswa
      $request->request->add(['user_id' => $user->id]);
      $siswa = \App\Siswa::create($request->all());
      if ($request->hasfile('avatar')) {
        $request->file('avatar')->move('images/', $request->file('avatar')->getClientOriginalName());
        $siswa->avatar = $request->file('avatar')->getClientOriginalName();
        $siswa->save();
      }
      return redirect('/siswa')->with('sukses', 'Data Berhasil Diinput');
    }

    //didalam funsi, parameter untuk mendeteksi id dari siswa ($id) dapat di ganti dengan memanggil model tersebut dan membuat objek dari model tersebut
    //public function edit($id)
    public function edit(Siswa $siswa)
    {
      //jika model sudah dideklarasikan di atas maka \App\ tidak usah di tulis kembali di dalam fungsi
      //$siswa = \App\Siswa::find($id);

      //dan jika dalam route {id} sudah di ganti dengan nama model {siswa} maka otomatis controller akan mencari id dengan sendirinya tanpa kita mencarinya dengan code di bawah
      //$siswa = Siswa::find($id);
      
      return view('siswa/edit', ['siswa' => $siswa]);
    }

    //public function update(Request $request, $id)
    public function update(Request $request, Siswa $siswa)
    {
      $this->validate($request, [
        'nama_depan'    => 'required|min:3',
        'nama_belakang' => 'required',
        'email'         => 'required|email|unique:users',
        'jenis_kelamin' => 'required',
        'agama'         => 'required',
        'avatar'        => 'mimes:jpeg,jpg,png',
      ]);

      //$siswa = \App\Siswa::find($id);
      //$siswa = Siswa::find($id);
      $siswa->Update($request->all());
      if ($request->hasfile('avatar')) {
        $request->file('avatar')->move('images/', $request->file('avatar')->getClientOriginalName());
        $siswa->avatar = $request->file('avatar')->getClientOriginalName();
        $siswa->save();
      }
      return redirect('/siswa')->with('sukses', 'Data Berhasil Diupdate');
    }

    //public function delete($id)
    public function delete(Siswa $siswa)
    {
      //$siswa = \App\Siswa::find($id);
      //$siswa = Siswa::find($id);
      $siswa->delete();
      return redirect('/siswa')->with('sukses', 'Data Berhasil Dihapus');
    }

    //public function profile($id)
    public function profile(Siswa $siswa)
    {
      //$siswa = \App\Siswa::find($id);
      $matapelajaran = \App\Mapel::all();

      //Menyiapkan data untuk chart
      $categories = [];
      $data = [];

      foreach ($matapelajaran as $mp) {
        if ($siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()) {
          $categories[] = $mp->nama;
          $data[] = $siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()->pivot->nilai;
        }
      }
      //dd($data);
      //dd(json_encode($categories));

      return view('siswa.profile', ['siswa' => $siswa, 'matapelajaran' => $matapelajaran, 'categories' => $categories, 'data' => $data]);
    }

    //public function addnilai(Request $request, $idsiswa)
    public function addnilai(Request $request, Siswa $siswa)
    {
      //$siswa = \App\Siswa::find($idsiswa);
      if ($siswa->mapel()->where('mapel_id', $request->mapel)->exists()) {
          //return redirect('siswa/'.$siswaid.'/profile')->with('error', 'Mata Pelajaran Sudah Ada');
          return redirect()->back()->with('error', 'Mata Pelajaran Sudah Ada');
      }
      $siswa->mapel()->attach($request->mapel, ['nilai' => $request->nilai]);
      //return redirect('siswa/'.$siswaid.'/profile')->with('sukses', 'Nilai Berhasil Dimasukkan');
      return redirect()->back()->with('sukses', 'Nilai Berhasil Dimasukkan');
    }

    //public function deletenilai($idsiswa, $idmapel)
    public function deletenilai(Siswa $siswa, $idmapel)
    {
      //$siswa = \App\Siswa::find($idsiswa);
      $siswa->mapel()->detach($idmapel);
      return redirect()->back()->with('sukses', 'Data Nilai Berhasil Dihapus');
    }

    public function exportExcel() 
    {
        return Excel::download(new SiswaExport, 'Siswa.xlsx');
    }

    public function exportPdf()
    {
      $siswa = \App\Siswa::all();
      $pdf = PDF::loadView('export.siswapdf', ['siswa' => $siswa]);
      return $pdf->download('Siswa.pdf');
    }
}
