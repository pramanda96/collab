<?php
use App\Siswa;
use App\Guru;

function Ranking5Besar()
{
    $siswa = siswa::all();
    $siswa->map(function($s){
      $s->RataRataNilai = $s->RataRataNilai();
      return $s;
    });
    $siswa = $siswa->sortByDesc('RataRataNilai')->take(5);
    return $siswa;
}

function TotalSiswa()
{
    return Siswa::count();
}

function TotalGuru()
{
    return Guru::count();
}